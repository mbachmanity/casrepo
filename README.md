## Enterprise Single Sign-On
 ##
    Java (Spring Webflow/MVC servlet) server component
    Pluggable authentication support (LDAP, database, X.509, 2-factor)
    Support for multiple protocols (CAS, SAML, OAuth, OpenID)
    Cross-platform client support (Java, .Net, PHP, Perl, Apache, etc)
    Integrates with uPortal, Liferay, BlueSocket, Moodle, and Google Apps to name a few

### CAS provides a friendly open source community that actively supports and contributes to the project. While the project is rooted in higher-ed open source, it has grown to an international audience spanning Fortune 500 companies and small special-purpose installations. ###